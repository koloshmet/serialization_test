mod data;
mod ser_de;

use std::time;
use std::fs;
use std::path;
use std::io;
use std::io::{Read, Write};

use prost::Message;
mod proto_data {
    include!(concat!(env!("OUT_DIR"), "/data.rs"));
}

fn user_to_protobuf(data: &data::User) -> proto_data::User {
    let mut user = proto_data::User::default();
    user.password_hash = String::from(&data.password_hash);
    user.volume_size = data.volume_size;
    for file in &data.files {
        user.files.push(file_to_protobuf(&file));
    }
    return user;
}

fn file_to_protobuf(data: &data::File) -> proto_data::File {
    let mut file = proto_data::File::default();
    file.name = String::from(&data.name);
    file.size = data.size;
    file.access_rate = data.access_rate;
    return file;
}

fn data_to_protobuf(data: &data::Data) -> proto_data::Data {
    let mut dt = proto_data::Data::default();
    for (name, user) in &data.users {
        dt.users.insert(String::from(name), user_to_protobuf(&user));
    }
    return dt;
}

fn bench<TFunc>(name: &str, func: TFunc, iterations: u32) where TFunc: Fn() -> () {
    let timer = time::Instant::now();
    for _ in 0..iterations {
        func();
    }
    println!(
        "{}\n  Average Time: {}ms",
        name,
        timer.elapsed().as_millis() / u128::from(iterations));
}

fn size(data: &data::Data) {
    let json = ser_de::to_json(&data);
    let yaml = ser_de::to_yaml(&data);
    let native = ser_de::to_native(&data);
    let mp = ser_de::to_mp(&data);
    let protobuf = data_to_protobuf(&data).encode_to_vec();
    println!("Serialization size");
    println!("Json\n  Size: {}B", json.len());
    println!("Yaml\n  Size: {}B", yaml.len());
    println!("Native\n  Size: {}B", native.len());
    println!("Message Pack\n  Size: {}B", mp.len());
    println!("Protobuf\n  Size: {}B", protobuf.len());
}

fn to_mem(data: &data::Data) {
    let json = || {
        ser_de::to_json(&data);
    };
    let yaml = || {
        ser_de::to_yaml(&data);
    };
    let native = || {
        ser_de::to_native(&data);
    };
    let mp = || {
        ser_de::to_mp(&data);
    };
    let pt = data_to_protobuf(&data);
    let protobuf = || {
        pt.encode_to_vec();
    };

    println!("Serialization to memory");
    bench("Json", json, 100);
    bench("Yaml", yaml, 100);
    bench("Native", native, 100);
    bench("Message Pack", mp, 100);
    bench("Protobuf", protobuf, 100);
}

fn open_fw(ext: &str) -> io::BufWriter<fs::File> {
    let path = path::PathBuf::from(format!("f.{}", ext));
    return io::BufWriter::new(fs::OpenOptions::new()
        .write(true).create(true).truncate(true)
        .open(path).unwrap());
}

fn open_fr(ext: &str) -> io::BufReader<fs::File> {
    let path = path::PathBuf::from(format!("f.{}", ext));
    return io::BufReader::new(fs::File::open(path).unwrap());
}

fn to_drive(data: &data::Data) {
    let json = || {
        let file = open_fw("json");
        serde_json::to_writer(file, &data).unwrap();
    };
    let yaml = || {
        let file = open_fw("yaml");
        serde_yaml::to_writer(file, &data).unwrap();
    };
    let native = || {
        let file = open_fw("bin");
        bincode::serialize_into(file, &data).unwrap();
    };
    let mp = || {
        let mut file = open_fw("msgpack");
        rmp_serde::encode::write(&mut file, &data).unwrap();
    };
    let pt = data_to_protobuf(&data);
    let protobuf = || {
        let mut file = open_fw("protobuf");
        let bts = pt.encode_to_vec();
        file.write_all(&bts);
    };

    println!("Serialization to drive");
    bench("Json", json, 100);
    bench("Yaml", yaml, 100);
    bench("Native", native, 100);
    bench("Message Pack", mp, 100);
    bench("Protobuf", protobuf, 100);
}

fn from_mem(data: &data::Data) {
    let json_value = ser_de::to_json(&data);
    let yaml_value = ser_de::to_yaml(&data);
    let native_value = ser_de::to_native(&data);
    let mp_value = ser_de::to_mp(&data);
    let pt_value = data_to_protobuf(&data).encode_to_vec();

    let json = || {
        serde_json::from_slice::<data::Data>(&json_value).unwrap();
    };
    let yaml = || {
        serde_yaml::from_slice::<data::Data>(&yaml_value).unwrap();
    };
    let native = || {
        bincode::deserialize::<data::Data>(&native_value).unwrap();
    };
    let mp = || {
        rmp_serde::from_slice::<data::Data>(&mp_value).unwrap();
    };
    let protobuf = || {
        let _val = proto_data::Data::decode(&*pt_value).unwrap();
    };
    println!("Deserialization from memory");
    bench("Json", json, 100);
    bench("Yaml", yaml, 100);
    bench("Native", native, 100);
    bench("Message Pack", mp, 100);
    bench("Protobuf", protobuf, 100);
}

fn from_drive() {
    let json = || {
        let file = open_fr("json");
        let _result: data::Data = serde_json::from_reader(file).unwrap();
    };
    let yaml = || {
        let file = open_fr("yaml");
        let _result: data::Data = serde_yaml::from_reader(file).unwrap();
    };
    let native = || {
        let file = open_fr("bin");
        let _result: data::Data = bincode::deserialize_from(file).unwrap();
    };
    let mp = || {
        let mut file = open_fr("msgpack");
        let _result: data::Data = rmp_serde::decode::from_read(&mut file).unwrap();
    };
    let protobuf = || {
        let mut file = open_fr("protobuf");
        let mut vc = vec![];
        file.read_to_end(&mut vc);
        let _val = proto_data::Data::decode(&*vc).unwrap();
    };

    println!("Deserialization from drive");
    bench("Json", json, 100);
    bench("Yaml", yaml, 100);
    bench("Native", native, 100);
    bench("Message Pack", mp, 100);
    bench("Protobuf", protobuf, 100);
}



fn main() {
    let sample_data = data::generate_sample_data(5000);

    size(&sample_data);
    to_mem(&sample_data);
    from_mem(&sample_data);
    to_drive(&sample_data);
    from_drive();
}

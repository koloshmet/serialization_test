use serde::{Serialize, Deserialize};

use std::collections;

#[derive(Serialize, Deserialize, Clone)]
pub(crate) struct File {
    pub(crate) name: String,
    pub(crate) size: u32,
    pub(crate) access_rate: f32
}

#[derive(Serialize, Deserialize, Clone)]
pub(crate) struct User {
    pub(crate) password_hash: String,
    pub(crate) volume_size: u64,
    pub(crate) files: Vec<File>
}


#[derive(Serialize, Deserialize)]
pub(crate) struct Data {
    pub(crate) users: collections::HashMap<String, User>
}

pub(crate) fn generate_sample_data(user_count: usize) -> Data {
    let file1 = File {
        name: String::from("tmp.txt"),
        size: 8192,
        access_rate: 0.5
    };
    let file2 = File {
        name: String::from("file.bin"),
        size: 10000,
        access_rate: 0.0002
    };
    let file3 = File {
        name: String::from("id_rsa.pub"),
        size: 4096,
        access_rate: 1.0
    };

    let user = User {
        password_hash: String::from("aaaa-bbbb"),
        volume_size: u64::pow(2, 16),
        files: vec![file1, file2, file3]
    };

    let mut data = collections::HashMap::<String, User>::new();
    for i in 0..user_count {
        data.insert(i.to_string(), user.clone());
    }

    return Data {
        users: data
    };
}

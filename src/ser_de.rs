use serde_json;
use serde_yaml;
use avro_rs;
use rmp_serde;
use bincode;

pub(crate) fn to_json<TData>(data: &TData) -> Vec<u8> where TData: serde::Serialize {
    return serde_json::to_vec(&data).unwrap();
}

pub(crate) fn to_yaml<TData>(data: &TData) -> Vec<u8> where TData: serde::Serialize {
    return serde_yaml::to_vec(&data).unwrap();
}

pub(crate) fn to_native<TData>(data: &TData) -> Vec<u8> where TData: serde::Serialize {
    return bincode::serialize(&data).unwrap();
}

pub(crate) fn to_mp<TData>(data: &TData) -> Vec<u8> where TData: serde::Serialize {
    return rmp_serde::to_vec(&data).unwrap();
}
